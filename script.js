const gEND_ROW_TABLE = -1;
// Biến toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gCOURSENAME_COL = 1;
const gLEVEL_COL = 2;
const gTEACHER_COL = 3;
const gDURATION_COL=4;
const gPRICE_COL=5;
const gDISCOUNT_COL=6;
const gACTION_COL =7 ;
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
var gDATA_COLS=["id","courseName","level","teacherName","duration","price","discountPrice","action"];

$(document).ready(function(){
    console.log(gCoursesDB.courses);
    onShowRecommend()
    onShowPopular();
    onShowTrending();
    $("#course-table").DataTable({
        data:gCoursesDB.courses,
        columns: [
        { data: gDATA_COLS[gID_COL] },
        { data: gDATA_COLS[gCOURSENAME_COL] },
        { data: gDATA_COLS[gLEVEL_COL] },
        { data: gDATA_COLS[gTEACHER_COL] },
        { data: gDATA_COLS[gDURATION_COL] },
        { data: gDATA_COLS[gPRICE_COL] },
        { data: gDATA_COLS[gDISCOUNT_COL] },
        { data: gDATA_COLS[gACTION_COL] }
        ],
        columnDefs: [
        { // định nghĩa lại cột action
            targets: gACTION_COL,
            defaultContent: 
            `
            <button id="btn-detail" class="btn btn-primary btn-detail" >Chi tiết</button>
            <button id="btn-delete" class="btn btn-danger btn-delete" >Xóa</button>
            `
        }
        ]
    });
    $("#read-course-modal").on("click","#btn-detail",function(){
        console.log("Update modal");
        $("#update-course-modal").modal("show");
        onBtnDetailClick(this);
        $("#read-course-modal").modal("hide");
    });
    $("#read-course-modal").on("click","#btn-create-user",function(){
        console.log("Create modal");
        $("#create-course-modal").modal("show");
        onBtnCreate();
        $("#read-course-modal").modal("hide");
    });
    var ID;
    $("#read-course-modal").on("click","#btn-delete",function(){
        console.log("Delete modal");
        ongetID(this);
        $("#delete-confirm-modal").modal("show");
        $("#read-course-modal").modal("hide");
    });
    $("#delete-confirm-modal").click("#btn-confirm-delete-course",function(){
        onBtnDeleteClick(ID);
    });
});
function onBtnCreate(){
    $("#create-course-modal").on("click","#btn-create-course",function(){    
        debugger;
        var po=false;
        var tr=false;
        console.log($("input[name='CradioType']:checked").val());
        if($("input[name='CradioType']:checked").val()=="Popular"){
            po=true;
            tr=false;
        } else if($("input[name='CradioType']:checked").val()=="Trending"){
            tr=true;
            po=false;
        }
        var obj={
            courseCode: $("#input-course-code").val(),
            courseName: $("#input-course-name").val(),
            coverImage: $("#input-course-img").val(),
            discountPrice: $("#input-course-price-discount").val(),
            duration: $("#input-course-duration").val(),
            id: getNextId(),   
            isPopular: po,
            isTrending: tr,
            level: $("#select-level").val(),
            price: $("#input-course-price").val(),
            teacherName: $("#input-teacher-name").val(),
            teacherPhoto: $("#input-teacher-img").val()
        }
        gCoursesDB.courses.push(obj);
        console.log(gCoursesDB.courses);
        $("#create-course-modal").modal("hide");
        var vTable=$("#course-table").DataTable();
        vTable.clear();
        vTable.rows.add(gCoursesDB.courses);
        vTable.draw();
    })
}
function ongetID(paramElement){
    debugger;
    var vRowClick = $(paramElement).closest("tr"); // Xác định tr chứa nút bấm được click
    var vTable = $('#course-table ').DataTable(); // tạo biện truy xuất đến datatable
    var vDataRow = vTable.row( vRowClick ).data();
    ID=vDataRow.id;
    console.log(ID);
}
$(document).on("click","#btn-read",function(){
    $("#read-course-modal").modal("show");
});
$("#read-course-modal").on("show.bs.modal",function(){
    var vTable=$("#course-table").DataTable();
    vTable.rows.add(gCoursesDB.courses);
    vTable.draw();
});

function onBtnDeleteClick(){
    var index=getIndexById(ID);
    console.log(index);
    gCoursesDB.courses.splice(index, 1);
    alert("Xóa voucher thành công!");
    console.log(gCoursesDB.courses);
    $("#delete-confirm-modal").modal("hide");
    var vTable=$("#course-table").DataTable();
    vTable.clear();
    vTable.rows.add(gCoursesDB.courses);
    vTable.draw();
    $("#read-course-modal").modal("show");
}
function getNextId() {
    var vNextId = 0;
    // Nếu mảng chưa có đối tượng nào thì Id = 1
    if(gCoursesDB.courses.length == 0) {
      vNextId = 1;
    }
  // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
else {
      vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
    }
    return vNextId;
  }

function onBtnDetailClick(paramElement){
    var vRowClick = $(paramElement).closest("tr"); // Xác định tr chứa nút bấm được click
    var vTable = $('#course-table ').DataTable(); // tạo biện truy xuất đến datatable
    var vDataRow = vTable.row( vRowClick ).data();
    $("#input-course-name-u").val(vDataRow.courseName);
    $("#input-course-code-u").val(vDataRow.courseCode);
    $("#select-level-u").val(vDataRow.level);
    $("#input-course-price-u").val(vDataRow.price);
    $("#input-course-price-discount-u").val(vDataRow.discountPrice);
    $("#input-course-duration-u").val(vDataRow.duration);
    $("#input-teacher-name-u").val(vDataRow.teacherName);
    $("#input-teacher-img-u").val(vDataRow.teacherPhoto);
    $("#input-course-img-u").val(vDataRow.coverImage);
    if(vDataRow.isPopular==true){
        $("#radio-popular-u").attr("checked",true);
    }
    if(vDataRow.isTrending==true){
        $("#radio-trending-u").attr("checked",true);
    }

    $("#update-course-modal").on("click","#btn-update-user",function(){
        console.log($("#input-course-name-u").val());
        console.log($("#input-course-code-u").val());
        console.log($("#select-level-u").val());
        console.log($("#input-course-price-u").val());
        console.log($("#input-course-price-discount-u").val());
        console.log($("#input-course-duration-u").val());
        console.log($("#input-teacher-name-u").val());
        console.log($("#input-teacher-img-u").val());
        console.log($("#input-course-img-u").val());
        vDataRow.courseName=$("#input-course-name-u").val();
        vDataRow.courseCode=$("#input-course-code-u").val();
        vDataRow.level=$("#select-level-u").val();
        vDataRow.price=$("#input-course-price-u").val();
        vDataRow.discountPrice=$("#input-course-price-discount-u").val();
        vDataRow.duration=$("#input-course-duration-u").val();
        vDataRow.teacherName=$("#input-teacher-name-u").val();
        vDataRow.teacherPhoto=$("#input-teacher-img-u").val();
        vDataRow.coverImage=$("#input-course-img-u").val();
        debugger;
        if($("input[name='radioType']:checked").val()=="popular"){
            vDataRow.isPopular=true;
            vDataRow.isTrending=false;
        } else if($("input[name='radioType']:checked").val()=="trending"){
            vDataRow.isTrending=true;
            vDataRow.isPopular=false;
        }
        console.log(vDataRow);
        $("#update-course-modal").modal("hide");
        $("#read-course-modal").modal("show");
    });
}
function getIndexById(paramId) {
    debugger;
    console.log(paramId);
    var vIndex = -1;
    var vFound = false;
    var vLoopIndex = 0;
    while(!vFound && vLoopIndex < gCoursesDB.courses.length) {
      if(gCoursesDB.courses[vLoopIndex].id === paramId) {
        vIndex = vLoopIndex;
        vFound = true;
      }
      else {
        vLoopIndex ++;
      }
    }
    return vIndex;
  }
function onShowTrending(){
    var i;
    var div=$("<div/>",{class:"row"});
    for(i=0;i<gCoursesDB.courses.length;i++){
        if( gCoursesDB.courses[i].isTrending==true ){
            index=i;
            var display= $("<div/>",{class:"card",style:"width: 18rem;"});
            var img = $('<img />', { 
                src: gCoursesDB.courses[i].coverImage,
                class:"card-img-top"
                });
            img.appendTo(display);
            var body=$("<div/>",{class:"card-body"})
            var pagrah=$("<h8 />",{
                class:"card-title",
                html:gCoursesDB.courses[i].courseName,
            });
            pagrah.css("color","blue");
            pagrah.appendTo(body);
        
            var duration=$("<p/>",{
                html:"<i class='fa-solid fa-clock'></i>"+gCoursesDB.courses[i].duration ,
            });
            var level=$("<p/>",{
                class:"level",
                html:gCoursesDB.courses[i].level
            });
            var divRow1=$("<div/>",{class:"row"});
            var divRow2=$("<div/>",{class:"row"});
            var Price=$("<p/>",{
                html:"<i class='fa-solid fa-dollar-sign'></i>"+gCoursesDB.courses[i].discountPrice 
            });
            var DisPrice=$("<p/>",{
                class:"price normal",
                html:gCoursesDB.courses[i].price 
            });
            var footer=$("<div/>",{class:"card-footer"});
            var small=$("<small/>",{
                class:"text-muted row"
            })
            var imgTeacher = $('<img />', { 
                class:"teacher-img",
                src: gCoursesDB.courses[i].teacherPhoto,
            });
            var icon=$("<i/>",{
                class:'fa-solid fa-bookmark fa-2x',
                id:"icon"
            })
            var name=$("<p/>",{
                class:"card-text",
                id:"nameTeacher",
                html:gCoursesDB.courses[i].teacherName
            });
            
            duration.appendTo(divRow1);
            level.appendTo(divRow1)
            Price.appendTo(divRow2);
            DisPrice.appendTo(divRow2);
            divRow1.appendTo(body);
            divRow2.appendTo(body);
            imgTeacher.appendTo(small);
            name.appendTo(small);
            icon.appendTo(small);
            small.appendTo(footer);
            footer.appendTo(body)
            body.appendTo(display);
            display.appendTo(div);
        }
    }
    
    div.appendTo($("#trending-card"));
}
function onShowPopular(){
    var i;
    var div=$("<div/>",{class:"row"});
    for(i=0;i<gCoursesDB.courses.length;i++){
        if( gCoursesDB.courses[i].isPopular==true ){
            index=i;
            var display= $("<div/>",{class:"card",style:"width: 18rem;"});
            var img = $('<img />', { 
                src: gCoursesDB.courses[i].coverImage,
                class:"card-img-top"
                });
            img.appendTo(display);
            var body=$("<div/>",{class:"card-body"})
            var pagrah=$("<h8 />",{
                class:"card-title",
                html:gCoursesDB.courses[i].courseName,
            });
            pagrah.css("color","blue");
            pagrah.appendTo(body);
        
            var duration=$("<p/>",{
                html:"<i class='fa-solid fa-clock'></i>"+gCoursesDB.courses[i].duration ,
            });
            var level=$("<p/>",{
                class:"level",
                html:gCoursesDB.courses[i].level
            });
            var divRow1=$("<div/>",{class:"row"});
            var divRow2=$("<div/>",{class:"row"});
            var Price=$("<p/>",{
                html:"<i class='fa-solid fa-dollar-sign'></i>"+gCoursesDB.courses[i].discountPrice 
            });
            var DisPrice=$("<p/>",{
                class:"price normal",
                html:gCoursesDB.courses[i].price 
            });
            var footer=$("<div/>",{class:"card-footer"});
            var small=$("<small/>",{
                class:"text-muted row"
            })
            var imgTeacher = $('<img />', { 
                class:"teacher-img",
                src: gCoursesDB.courses[i].teacherPhoto,
            });
            var icon=$("<i/>",{
                class:'fa-solid fa-bookmark fa-2x',
                id:"icon"
            })
            var name=$("<p/>",{
                class:"card-text",
                id:"nameTeacher",
                html:gCoursesDB.courses[i].teacherName
            });
            
            duration.appendTo(divRow1);
            level.appendTo(divRow1)
            Price.appendTo(divRow2);
            DisPrice.appendTo(divRow2);
            divRow1.appendTo(body);
            divRow2.appendTo(body);
            imgTeacher.appendTo(small);
            name.appendTo(small);
            icon.appendTo(small);
            small.appendTo(footer);
            footer.appendTo(body)
            body.appendTo(display);
            display.appendTo(div);
        }
    }
    
    div.appendTo($("#popular-card"));
}
function onShowRecommend(){
    var div=$("<div/>",{class:"row"});
    for(let i = 0; i < gCoursesDB.courses.length; i++)
	{
        var display= $("<div/>",{id:"Re-card" ,class:"card",style:"width: 18rem;"});
		var img = $('<img />', { 
            src: gCoursesDB.courses[i].coverImage,
            class:"card-img-top"
          });
        img.appendTo(display);
        var body=$("<div/>",{class:"card-body"})
        var pagrah=$("<h8 />",{
            class:"card-title",
            html:gCoursesDB.courses[i].courseName,
        });
        pagrah.css("color","blue");
        pagrah.appendTo(body);

        var duration=$("<p/>",{
            html:"<i class='fa-solid fa-clock'></i>"+gCoursesDB.courses[i].duration ,
        });
        var level=$("<p/>",{
            class:"level",
            html:gCoursesDB.courses[i].level
        });
        var divRow1=$("<div/>",{class:"row"});
        var divRow2=$("<div/>",{class:"row"});
        var Price=$("<p/>",{
            html:"<i class='fa-solid fa-dollar-sign'></i>"+gCoursesDB.courses[i].discountPrice 
        });
        var DisPrice=$("<p/>",{
            class:"price normal",
            html:gCoursesDB.courses[i].price 
        });
        var footer=$("<div/>",{class:"card-footer"});
        var small=$("<small/>",{
            class:"text-muted row"
        })
        var imgTeacher = $('<img />', { 
            class:"teacher-img",
            src: gCoursesDB.courses[i].teacherPhoto,
        });
        var icon=$("<i/>",{
            class:'fa-solid fa-bookmark fa-2x',
            id:"icon"
        })
        var name=$("<p/>",{
            class:"card-text",
            id:"nameTeacher",
            html:gCoursesDB.courses[i].teacherName
        });
        
        duration.appendTo(divRow1);
        level.appendTo(divRow1)
        Price.appendTo(divRow2);
        DisPrice.appendTo(divRow2);
        divRow1.appendTo(body);
        divRow2.appendTo(body);
        imgTeacher.appendTo(small);
        name.appendTo(small);
        icon.appendTo(small);
        small.appendTo(footer);
        footer.appendTo(body)
        body.appendTo(display);
        display.appendTo(div);
	}
    div.appendTo($("#recommend-card"));
}

